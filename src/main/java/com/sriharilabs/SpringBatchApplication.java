package com.sriharilabs;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.microsoft.azure.spring.data.cosmosdb.repository.config.EnableDocumentDbRepositories;

@SpringBootApplication
@EnableBatchProcessing
@EnableDocumentDbRepositories
public class SpringBatchApplication implements CommandLineRunner {

	@Autowired
	MyService myService;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBatchApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		//myService.add();
		myService.getcludId();
		myService.getAll();
	}
	
}
