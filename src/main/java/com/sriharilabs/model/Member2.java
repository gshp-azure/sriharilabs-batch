package com.sriharilabs.model;

import org.springframework.data.annotation.Id;

import com.microsoft.azure.documentdb.IndexingMode;
import com.microsoft.azure.spring.data.cosmosdb.core.mapping.Document;
import com.microsoft.azure.spring.data.cosmosdb.core.mapping.DocumentIndexingPolicy;

import lombok.Data;

@Data
@Document(collection = "member2")
@DocumentIndexingPolicy(mode = IndexingMode.Lazy)
public class Member2 {
	 @Id
	    private String id;
	private String userName;
	private String zonelCode;
	private String cludId;
//	
//	 @Override
//	    public String toString() {
//	        return String.format("%s: %s %s %s", this.userName, this.zonelCode, this.cludId);
//	    }
}
