package com.sriharilabs.step;

import java.util.List;

import org.springframework.batch.item.ItemWriter;

import com.sriharilabs.model.Member2;

import reactor.core.publisher.Flux;


public class Writer implements ItemWriter<Flux<Member2>> {

	

	@Override
	public void write(List<? extends Flux<Member2>> items) throws Exception {
		
		
		items.forEach(s->{
				s.log()
			      .subscribe();
		});
	}

	
}