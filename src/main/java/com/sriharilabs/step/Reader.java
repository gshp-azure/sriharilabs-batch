package com.sriharilabs.step;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.sriharilabs.model.Member;

import reactor.core.publisher.Flux;

public class Reader implements ItemReader<Flux<Member>> {

	private String[] messages = { "javainuse.com",
			"Welcome to Spring Batch Example",
			"We use H2 Database for this example" };

	private int count = 0;

	@Override
	public Flux<Member> read() throws Exception, UnexpectedInputException,
			ParseException, NonTransientResourceException {

		System.out.println(Thread.currentThread().getName());
		if (count < messages.length-1) {
			
			System.out.println(Thread.currentThread().getName()+ "gshp.........1  read:: "+messages[count++]);
			//Thread.currentThread().sleep(10000);
			Member m=new Member();
			m.setCludId(""+count);
			m.setUserName("gsrihari"+count);
			m.setZonelCode("Zid"+count);
			Flux f=Flux.just(m);
			return f; 
		} else {
			count = 0;
		}
		return null;
	}

}