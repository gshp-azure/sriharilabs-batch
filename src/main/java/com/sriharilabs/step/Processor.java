package com.sriharilabs.step;

import org.springframework.batch.item.ItemProcessor;

import com.sriharilabs.model.Member;
import com.sriharilabs.model.Member2;

import reactor.core.publisher.Flux;

public class Processor implements ItemProcessor<Flux<Member>, Flux<Member2>> {

	@Override
	public Flux<Member2> process(Flux<Member> data) throws Exception {
		
		System.out.println(Thread.currentThread().getName()+"gshp.........2  process:: "+data);
		
//		Flux<String> res=data
//							.log()
//							.map(s->{
//							return s.toUpperCase();
//							});
//							
		
		Member2 m2=new Member2();
		
		data
		.log()
		.subscribe(d->{
			m2.setCludId(d.getCludId());
			m2.setUserName(d.getUserName());
			m2.setZonelCode(d.getZonelCode());
		});
		
		
		return Flux.just(m2);
	}

}
