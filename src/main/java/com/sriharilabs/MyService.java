package com.sriharilabs;

import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sriharilabs.model.Member2;
import com.sriharilabs.repository.MemberRepository;

@Service
public class MyService {
	@Autowired
	MemberRepository memberRepository;
	public void add() {
		
		IntStream.range(1, 5).forEach(s->{
			
			Member2 m=new Member2();
			m.setCludId(""+s);
			m.setUserName("srihari"+s);
			m.setZonelCode(""+s);
			
			memberRepository.save(m);
			
		});
		
		
	}
	
	public void getAll() {
		memberRepository.findAll()
						.forEach(s->{
							System.out.println("findALL    "+" username: "+s.getUserName()+ "      clubid"+s.getCludId());
						});
	}
	public void getcludId() {
		memberRepository.findBycludId(""+2)
						.forEach(s->{
			
						System.out.println("clubid... "+ s.getUserName()+"   "+s.getCludId());
						});
		
	}

}
